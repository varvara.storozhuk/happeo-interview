const {Builder, By, Key} = require('selenium-webdriver');

(async function example() {
    let driver = await new Builder().forBrowser('chrome').build();
    await driver.manage().setTimeouts( { implicit: 5000 } );

    console.log("1. Navigate to URL provided");
    await driver.get('https://staging.unvrs.io/');

    let email = 'interview-varvara@apps.fi'
    let password = 'G6rdE&a&'

    await driver.findElement(By.id('signin-btn')).click();
    await driver.findElement(By.id('identifierId')).sendKeys(email, Key.ENTER);
    await driver.sleep(2000);
    await driver.findElement(By.css('input[type=\'password\']')).sendKeys(password, Key.ENTER);
    await driver.sleep(10000);

    console.log("2. Open Channels menu");
    await driver.findElement(By.id("nav-channels-button")).click();

    console.log("3. Search for channel “Interview Channel” ");
    await driver.findElement(By.id("nav-channels-search")).sendKeys('Interview Channel', Key.ENTER);
    await driver.sleep(2000);

    console.log("4. Navigate to the channel by clicking on the search result");
    await driver.findElement (By.xpath("//*[contains(text(),'Interview Channel')]")).click();

    console.log("5. Open a random post (top right 3 dots menu -> View post)");
    await driver.findElement (By.css("div[data-tracker='Post actions menu']")).click();
    await driver.findElement (By.css("*[data-tracker='View post']")).click();

    console.log("6. Add a random reaction from the default reactions list");
    await driver.findElement (By.xpath("//*[contains(text(),'Like')]/ancestor::*[self::button]")).click();

    console.log("Verify that reaction was submited");
    try {
        await driver.findElement(By.css("a[data-tracker='reactions-summary']"));
        console.log('Passed')
    } catch {
        console.log('Failed')
    }

    driver.quit();
})();