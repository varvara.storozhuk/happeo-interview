const {Builder, By, Key} = require('selenium-webdriver');

(async function example() {
    let driver = await new Builder().forBrowser('chrome').build();
    await driver.manage().setTimeouts( { implicit: 5000 } );

    console.log("1. Navigate to URL provided");
    let email = 'interview-varvara@apps.fi'
    let password = 'G6rdE&a&'
    await driver.get('https://staging.unvrs.io/');

    await driver.findElement(By.id('signin-btn')).click();
    await driver.findElement(By.id('identifierId')).sendKeys(email, Key.ENTER);
    await driver.sleep(2000);
    await driver.findElement(By.css('input[type=\'password\']')).sendKeys(password, Key.ENTER);
    await driver.sleep(10000);

    console.log("2. Navigate to Interview channel");
    await driver.findElement(By.id("nav-channels-button")).click();
    await driver.findElement(By.id("nav-channels-search")).sendKeys('Interview Channel', Key.ENTER);
    await driver.findElement (By.xpath("//*[contains(text(),'Interview Channel')]")).click();
    await driver.sleep(1000);

    console.log("3. Create a new post");
    let postText = 'Hello world'
    await driver.findElement (By.className("fr-element fr-view")).sendKeys(postText);
    await driver.findElement (By.xpath("//*[contains(text(),'Post options')]")).click();
    await driver.findElement (By.css("*[data-tracker='Publish']")).click();
    await driver.findElement (By.className("dropdown__control css-yk16xz-control")).click();
    await driver.findElement (By.id("react-select-2-option-4")).click();
    await driver.findElement (By.xpath("//div[@class='DayPickerInput']/div[1]/child::input")).click();

    const date = '27/08/2021';
    for (let i = 0; i < date.length; i++) {
        await driver.findElement (By.xpath("//div[@class='DayPickerInput']/div[1]/child::input")).click();
        await driver.findElement (By.xpath("//div[@class='DayPickerInput']/div[1]/child::input")).sendKeys(date.substr(i, 1));
    }

    await driver.findElement (By.xpath("//*[contains(text(),'Share')]/ancestor::*[self::button]")).click();
    await driver.findElement (By.xpath("//*[contains(text(),'Share')]/ancestor::*[self::button]")).click();

    console.log("Verify that announcement was posted");
    await driver.findElement (By.xpath("//li[text()=\"Announcements\"]")).click();
    try {
        await driver.findElement (By.xpath("//*[contains(text(),postText)]"));
        console.log('Passed')
    } catch {
        console.log('Failed')
    }

    driver.quit();
})();