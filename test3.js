const {Builder, By, Key} = require('selenium-webdriver');

(async function example() {
    let driver = await new Builder().forBrowser('chrome').build();
    await driver.manage().setTimeouts( { implicit: 5000 } );

    console.log("1. Navigate to URL provided");
    await driver.get('https://staging.unvrs.io/');

    let email = 'interview-varvara@apps.fi'
    let password = 'G6rdE&a&'

    await driver.findElement(By.id('signin-btn')).click();
    await driver.findElement(By.id('identifierId')).sendKeys(email, Key.ENTER);
    await driver.sleep(2000);
    await driver.findElement(By.css('input[type=\'password\']')).sendKeys(password, Key.ENTER);
    await driver.sleep(10000);

    console.log("2. Navigate to Pages");
    await driver.findElement(By.id("nav-pages-button")).click();
    await driver.findElement(By.id("nav-pages-search")).sendKeys('This Page Group', Key.ENTER);
    await driver.findElement (By.xpath("//*[contains(text(),'This Page Group')]")).click();

    console.log("3. Open Edit menu");
    await driver.findElement (By.className("page-edit-btn ng-isolate-scope")).click();

    console.log("3. Open Manage Pages");
    await driver.findElement (By.className("page-settings__actions-list ng-scope")).click();

    console.log("3. Add new page nested under First Page");
    await driver.findElement (By.className("block text-center")).click();
    await driver.findElement (By.id("newPageModalInput")).sendKeys('New page');
    await driver.findElement (By.className("level-1")).click();
    await driver.findElement (By.css("button[type='submit']")).click();

    console.log("Verify that new page settings are opened");
    try {
        await driver.findElement(By.className("page-settings--main"));
        console.log('Passed')
    } catch {
        console.log('Failed')
    }

    driver.quit();
})();